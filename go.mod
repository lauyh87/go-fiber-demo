module gitlab.com/lauyh87/go-fiber-demo

go 1.16

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gofiber/fiber v1.14.6
	github.com/gofiber/fiber/v2 v2.14.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.13.1 // indirect
	github.com/valyala/fasthttp v1.28.0 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.12 // indirect
)
