package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/lauyh87/go-fiber-demo/controllers"
)

func Setup(app *fiber.App) {
	app.Get("/", controllers.Hello)
	app.Post("/api/add-book", controllers.AddBook)
	// app.Post("/api/get-all-book")
	// app.Post("/api/get-book-by-id")
	// app.Post("/api/remove-book")
	app.Post("/api/register", controllers.Register)
	app.Post("/api/login", controllers.Login)
	app.Get("/api/user", controllers.User)
	app.Get("/api/logout", controllers.Logout)
}
