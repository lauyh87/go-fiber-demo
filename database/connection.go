package database

import (
	"gitlab.com/lauyh87/go-fiber-demo/models"
	"gitlab.com/lauyh87/go-fiber-demo/lib"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB



func Connect() {
	dsn := Lib.FetchConf("DSN")

	connection, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Could not connect to database") // log the error and exit the program
	}
	DB = connection

	// database migration
	connection.AutoMigrate(&models.Book{})
	connection.AutoMigrate(&models.Users{})
}
