package controllers

import (
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/lauyh87/go-fiber-demo/database"
	"gitlab.com/lauyh87/go-fiber-demo/models"
	"golang.org/x/crypto/bcrypt"
	"gitlab.com/lauyh87/go-fiber-demo/lib"
)

var SecretKey = Lib.FetchConf("SECRET")

func Register(c *fiber.Ctx) error {
	var data map[string]string // use hash table to hold the incoming data
	if err := c.BodyParser(&data); err != nil {
		return err
	}

	password, _ := bcrypt.GenerateFromPassword([]byte(data["Password"]), 14)
	user := models.Users{
		Name:     data["Name"],
		Email:    data["Email"],
		Password: password,
	}

	database.DB.Create(&user)
	return c.JSON(user)
}

func Login(c *fiber.Ctx) error {
	var data map[string]string
	// parse the hahtable data as json
	if err := c.BodyParser(&data); err != nil {
		return err
	}

	var user models.Users                                      // initialize the user variable
	database.DB.Where("email = ?", data["Email"]).First(&user) // search for the user and asign to the user variable
	if user.Id == 0 {
		c.Status(fiber.StatusNotFound)
		return c.JSON(fiber.Map{"message": "User not found"})
	}

	// compare password
	if err := bcrypt.CompareHashAndPassword(user.Password, []byte(data["Password"])); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"message": "Incorrect password"})
	}

	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Issuer:    strconv.Itoa(int(user.Id)),
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
	})

	token, err := claims.SignedString([]byte(SecretKey))

	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"message": "Could not login"})
	}

	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    token,
		Expires:  time.Now().Add(time.Hour * 24),
		HTTPOnly: true,
	}

	c.Cookie(&cookie)
	return c.Status(fiber.StatusOK).JSON(fiber.Map{"message": "Success"})
}

func User(c *fiber.Ctx) error {
	cookies := c.Cookies("jwt")
	token, err := jwt.ParseWithClaims(cookies, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKey), nil
	})

	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"msg": "User is not authenticated"})
	}

	claims := token.Claims.(*jwt.StandardClaims)
	var user models.Users
	database.DB.Where("id = ?", claims.Issuer).First(&user)
	return c.Status(fiber.StatusOK).JSON(user)
}

func Logout(c *fiber.Ctx) error {
	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    "",
		Expires:  time.Now().Add(-time.Hour),
		HTTPOnly: true,
	}
	c.Cookie(&cookie)
	return c.Status(fiber.StatusOK).JSON(fiber.Map{"message": "Success"})
}
