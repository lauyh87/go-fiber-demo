package controllers

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/lauyh87/go-fiber-demo/database"
	"gitlab.com/lauyh87/go-fiber-demo/models"
)


// test endpoint
func Hello(c *fiber.Ctx) error {
	return c.SendString("Hello")
}

func AddBook(c *fiber.Ctx) error {

	cookies := c.Cookies("jwt")
	_, err := jwt.ParseWithClaims(cookies, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKey), nil
	})

	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"msg": "User is not authenticated"})
	}

	// map JSON body to go lang map
	var data map[string]string
	if err := c.BodyParser(&data); err != nil {
		return err
	}
	
	book := models.Book{
		BookName: data["bookName"],
		AuthorName: data["authorName"],
	}

	database.DB.Create(&book)
	return c.JSON(book)
}
