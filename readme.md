# About This Project
This is a mini project for me to familiarize with Go language and using it to authenticate an user and simple endpoint for adding book.

## Notes
This project is using Go Fiber web framework and GORM (dialect - PostgreSQL)

Command to run the project: `go run main.go`

### Prerequisite
- Install go language development kit in your system with version == 1.16
- Install PostgreSQL in your machine.

# Documentation of the API
The API is documented by using Postman. You can find the documentation [here](https://documenter.getpostman.com/view/9794957/U16gP6zQ)