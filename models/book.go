package models

type Book struct {
	Id         int    `json:"-"`
	BookName   string `json:"book_name"`
	AuthorName string `json:"author_name"`
}
