package models

type Users struct {
	Id       uint   `json:"-"` // dun pass the id to frontend
	Name     string `json:"name"`
	Email    string `json:"email" gorm:"email"`
	Password []byte `json:"-"`
}
